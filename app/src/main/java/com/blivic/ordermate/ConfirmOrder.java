package com.blivic.ordermate;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.blivic.ordermate.Adapters.MenuAdapter;
import com.blivic.ordermate.Buttons.Fab;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.ArrayList;
import java.util.List;


public class ConfirmOrder extends ActionBarActivity {

    ListView list;
    private List<String> menuList;
    Fab mFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);

        TextView textView = (TextView) findViewById(R.id.confirm_tv);
        textView.setText(MenuStructure.RestaurantName);

        mFab = (Fab)findViewById(R.id.fabbutton);
        mFab.setFabDrawable(getResources().getDrawable(R.drawable.ic_menulist_fab));
        mFab.setFabColor(getResources().getColor(R.color.app_theme));

        mFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DisplayQR.class);
                startActivity(intent);
            }
        });

        menuList = new ArrayList<>();
        //Add only selected entries. That is entries with >0 choices.
        for(int i = 0;i<MenuStructure.option_count;i++)
            if(MenuStructure.menu_choice[i]>0)
                menuList.add(MenuStructure.getOption(i));   
        
        
        final MenuAdapter adapter = new MenuAdapter(menuList,this);
        list=(ListView)findViewById(R.id.menuList);
        list.setAdapter(adapter);

        list.setOnScrollListener(new AbsListView.OnScrollListener(){
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if(scrollState == SCROLL_STATE_TOUCH_SCROLL)
                {
                    Log.i("a", "scrolling touch...");
                    mFab.hideFab();
                }
                else if(scrollState ==  SCROLL_STATE_IDLE)
                    mFab.showFab();
            }
        });        
    }
}