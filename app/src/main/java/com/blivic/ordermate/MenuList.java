package com.blivic.ordermate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kanishthkarthik on 03/02/15.
 */
public class MenuList extends ArrayAdapter<MenuStructure> {
    /* Class that adds content to the FeedbackPage which extends user Class RatingStructure
    Every questions answer  is also saved in RatingStructure class's objects form here!
     */

    private List<MenuStructure> mcqList = new ArrayList<>();
    private Context context;
    private MenuStructure menuStructure;

    public MenuList(List<MenuStructure> nextmcqList, Context cxt) {
        super(cxt, R.layout.template_menu, nextmcqList);
        this.mcqList = nextmcqList;
        this.context = cxt;
    }

    @Override
    //Yipee!! Turns out this is called every time u scroll!!!!!!!
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) { // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.template_menu, null, true);
        }

        TextView menu_option_tv = (TextView) view.findViewById(R.id.menu_option_tv);
        menuStructure = mcqList.get(position);
        //menu_option_tv.setText(menuStructure.menu_option);
        return view;
    }
}
