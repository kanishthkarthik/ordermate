package com.blivic.ordermate;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kanishthkarthik on 03/02/15.
 */
public class ServerConfiguration {

    final static String MyPREFERENCES = "Myprefs";
    static final String defaultIP = "0.0.0.0";

    //For more on storing data after app cycle look up SharedPreferences


    public static SharedPreferences getPrefs(Context context)
    {
        return context.getSharedPreferences(MyPREFERENCES,
                context.MODE_PRIVATE);
    }

    //Valuename - "ServerIP"
    //Valuename - "port"

    //Returns String stored with name valueName
    public static String getString(String valuename, Context context) {
        return getPrefs(context).getString(valuename,defaultIP);
    }

    //Stores String stored with name valueName
    public static void setString(String valuename, Context context, String value) {
        getPrefs(context).edit().putString(valuename, value).commit();
    }
    public static int getint(String valuename, Context context, int defaultvalue) {
        return getPrefs(context).getInt(valuename,defaultvalue);
    }
    public static void setint(String valuename,Context context, int value) {
        getPrefs(context).edit().putInt(valuename, value).commit();
    }
}