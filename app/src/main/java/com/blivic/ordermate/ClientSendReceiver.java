package com.blivic.ordermate;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by kanishthkarthik on 14/02/15.
 */
public class ClientSendReceiver {

    /* This class sends and receives data between calling function and server look up 'Server Sockets in Java' to
    understand more! It follows TCP/IP,
     */

    public static String requestToServer(Context context,String requestCode, String data)
    {
        Socket socket;
        PrintWriter out;
        BufferedReader in;
        String line = "";

        try {
            socket = new Socket(ServerConfiguration.getString("ServerIP",context), ServerConfiguration.getint("port",context,5267));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            out.println(requestCode);
            out.println(data);
            out.flush();

            line = in.readLine();

            out.close();
            socket.close();
        }catch (UnknownHostException e) {
            Toast.makeText(context, "Don't know about host: hostname", Toast.LENGTH_SHORT).show();
        } catch (IOException e){
            Toast.makeText(context, "Couldn't get I/O for the connection to: hostname", Toast.LENGTH_SHORT).show();
        }
        return line;
    }

    public static void sendValue(int overall, Context context){

        Socket socket;
        PrintWriter out;
        JSONObject obj;
        /*
        try {
            socket = new Socket(ServerConfiguration.getString("ServerIP",context), ServerConfiguration.getint("port",context,5267));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

            //So that initial 0's issue is solved
            int data = 9;
            for(int i=0;i<RatingStructure.question_count;i++)
                data = data*10 + RatingStructure.answer[i];
            data = data*10 + overall;

            obj = new JSONObject();

            try {
                obj.put("waiter name", RatingStructure.waiter_name);
                obj.put("username", RatingStructure.name);
                obj.put("user email", RatingStructure.email);
                obj.put("data", new Integer(data));

            }catch (org.json.JSONException e) {
                Toast.makeText(context, "JSON Error", Toast.LENGTH_SHORT).show();
            }

            out.println("Request:03");
            out.println(obj);
            //Terminate server recieving process
            out.println("EOF");

            out.close();
            socket.close();
        }catch (UnknownHostException e) {
            //TODO: write obj to pending
            Toast.makeText(context, "Don't know about host: hostname", Toast.LENGTH_SHORT).show();
        } catch (IOException e){
            //TODO: write obj to pending
            Toast.makeText(context, "Couldn't get I/O for the connection to: hostname", Toast.LENGTH_SHORT).show();
        }
        */
    }
}