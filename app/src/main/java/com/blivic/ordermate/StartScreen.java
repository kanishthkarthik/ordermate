package com.blivic.ordermate;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;


public class StartScreen extends ActionBarActivity {

    private int tapCount;
    private long systemClockTime;
    private boolean stateChange;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);

        //Hide's the action bar for current screen(start screen)
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        stateChange = false;

        //call home screen after 1000 milliseconds
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                //Call home only if no double tap for settings page
                if (!stateChange) {
                    Intent intent;
                    //If no IP was entered go to set IP page(Setup)
                    if (ServerConfiguration.getString("ServerIP", getApplicationContext()).equalsIgnoreCase(ServerConfiguration.defaultIP))
                        intent = new Intent(StartScreen.this, Setup.class);

                        //If IP was previously set go to Choose waiter Page(ServerChoice)
                    else {
                        //TODO: temp                    
                        // intent = new Intent(StartScreen.this, ScannerActivity.class);

                        //Remove rest till }
                        intent = new Intent(StartScreen.this, MenuScreen.class);
                        intent.putExtra("HotelID", "Request:01");
                        startActivity(intent);
                    }
                    startActivity(intent);
                    finish();
                }
                //1000 is the wait time to show feedback (i.e.1ms)
            }
        }, 800);

        //Number of taps
        tapCount = 0;
        TextView tv = (TextView)findViewById(R.id.startScreen_text);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                switch (tapCount) {
                    case 0:
                        //First tap now and current time is stored in systemClockTime
                        systemClockTime = Calendar.getInstance().getTimeInMillis();
                        tapCount++;
                        break;
                    case 1:
                        //Second tap now and current time is - systemClockTime is checked for if lesser than 900milliseconds
                        if (Calendar.getInstance().getTimeInMillis() - systemClockTime < 800) {

                            stateChange = true;
                            //stateChanged is set so standard intent is not called and call Setup page!
                            Intent intent = new Intent(getApplicationContext(), Setup.class);
                            startActivity(intent);
                            finish();
                        } else
                            tapCount = 0;
                        break;
                }
            }
        });

    }
}
