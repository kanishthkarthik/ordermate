package com.blivic.ordermate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kanishthkarthik on 28/02/15.
 */

public class MenuStructure {

    /* All the information of users and their answers are saved here for every user and overwritten at every
     cycle, i.e. when the next information is entered!
      */
    static String RestaurantName = "";
    static int option_count;
    static int menu_choice[];
    private static List<String> menu_option = new ArrayList<String>();

    static void setOption(String m_o)
    {
        menu_option.add(m_o);
    }
    public static String getOption(int position)
    {
        return menu_option.get(position);        
    }

    //Initialize all answers to 0! Required in case option is left un-answered!
    static void setOption_count(int m_count) {
        option_count = m_count;
        menu_choice = new int[option_count];
        for(int i=0;i<option_count;i++)
            menu_choice[i] = 0;
    }
    public static int getChoiceCount(int position)
    {
        return menu_choice[position];
    }
}