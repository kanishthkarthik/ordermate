package com.blivic.ordermate.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.blivic.ordermate.MenuStructure;
import com.blivic.ordermate.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kanishthkarthik on 03/02/15.
 */
public class MenuAdapter extends ArrayAdapter<String> {
    /* Class that adds content to the FeedbackPage which extends user Class RatingStructure
    Every questions answer  is also saved in RatingStructure class's objects form here!
     */

    private List<String> mcqList = new ArrayList<>();
    private Context context;

    public MenuAdapter(List<String> menuList, Context cxt) {
        super(cxt, R.layout.template_menu, menuList);
        this.mcqList = menuList;
        this.context = cxt;
    }

    @Override
    //Yipee!! Turns out this is called every time u scroll!!!!!!!
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) { // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.template_menu, null, true);
        }

        TextView menu_option_tv = (TextView) view.findViewById(R.id.menu_option_tv);
        TextView menu_option_count = (TextView) view.findViewById(R.id.menu_option_count);
        menu_option_tv.setText(mcqList.get(position));
        if(MenuStructure.getChoiceCount(position)>0)
            menu_option_count.setText("x" + MenuStructure.getChoiceCount(position));
        else
        /*
        Mandatory to avoid randomly entered values due to listView entities being recycled!
         */
            menu_option_count.setText("");
        return view;
    }
}
