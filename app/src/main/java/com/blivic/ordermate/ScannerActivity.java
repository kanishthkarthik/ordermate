package com.blivic.ordermate;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


public class ScannerActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        IntentIntegrator integrator = new IntentIntegrator(ScannerActivity.this);
        integrator.initiateScan();

     }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        //To permit networking on main thread!
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            TextView tv = (TextView) findViewById(R.id.scanner_tv);
            String hotelID = intent.getStringExtra("SCAN_RESULT");

            tv.setText("Error!");

            Intent intent2 = new Intent(getApplicationContext(), MenuScreen.class);
            intent2.putExtra("HotelID",hotelID);
            startActivity(intent2);
            finish();
        }
        // else continue with any other code you need in the method
    }
}
