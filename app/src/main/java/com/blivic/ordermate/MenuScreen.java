package com.blivic.ordermate;

import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.blivic.ordermate.Adapters.MenuAdapter;
import com.blivic.ordermate.Buttons.Fab;

import java.util.ArrayList;
import java.util.List;

public class MenuScreen extends ActionBarActivity {

    ListView list;
    private List<String> menuList;
    Fab mFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_screen);

        mFab = (Fab)findViewById(R.id.fabbutton);
        mFab.setFabDrawable(getResources().getDrawable(R.drawable.ic_menulist_fab));
        mFab.setFabColor(getResources().getColor(R.color.app_theme));

        mFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ConfirmOrder.class);
                startActivity(intent);
            }
        });

        //To allow server handling on main thread
        //TODO: must be avoided to remove lag/screenFreeze while searching
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
        final String requestCode = "Request:01";
        Intent intent = getIntent();
        String hotelID = intent.getStringExtra("HotelID");
        String serverString = ClientSendReceiver.requestToServer(getApplicationContext(),requestCode,hotelID);

        int option_count= 0;

        menuList = new ArrayList<>();
        //TODO: Receive options line by line
        String word = "";
        for(int i = 0;i<serverString.length();i++)
        {
            char a = serverString.charAt(i);
            if(a==',')
            {
                if(option_count==0)
                {
                    MenuStructure.RestaurantName = word;
                    word = "";
                    option_count++;
                    continue;
                }
                menuList.add(word);
                MenuStructure.setOption(word);
                //Toast.makeText(getApplicationContext(), word, Toast.LENGTH_SHORT).show();
                word = "";
                option_count++;
            }
            else
                word+=a;
        }
        MenuStructure.setOption_count(option_count);

        final MenuAdapter adapter = new MenuAdapter(menuList,this);
        list=(ListView)findViewById(R.id.menuList);
        list.setAdapter(adapter);

        list.setOnScrollListener(new AbsListView.OnScrollListener(){
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if(scrollState == SCROLL_STATE_TOUCH_SCROLL)
                {
                    Log.i("a", "scrolling touch...");
                    mFab.hideFab();
                }
                else if(scrollState ==  SCROLL_STATE_IDLE)
                    mFab.showFab();
            }
        });

        //Detecting Swipes
        final SwipeDetector swipeDetector = new SwipeDetector();
        list.setOnTouchListener(swipeDetector);

        //If any of the ratings are changed the update corresponding answer in our array of answers!
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {                

                TextView menu_option_count = (TextView) view.findViewById(R.id.menu_option_count);
                
                if (swipeDetector.swipeDetected()) {
                    if (swipeDetector.getAction() == SwipeDetector.Action.LR) {

                        MenuStructure.menu_choice[i]++;
                        menu_option_count.setText("x" + MenuStructure.menu_choice[i]);

                    }
                    if (swipeDetector.getAction() == SwipeDetector.Action.RL) {
                        
                        MenuStructure.menu_choice[i] = MenuStructure.menu_choice[i]<=0?0:MenuStructure.menu_choice[i]-1;
                        menu_option_count.setText("x" + MenuStructure.menu_choice[i]);

                    }
                }
            }
        });
    }
}