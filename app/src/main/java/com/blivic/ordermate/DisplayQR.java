package com.blivic.ordermate;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

/*
    Class with activity activity_display_qr that displays the final QR code 
 */
public class DisplayQR extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_qr);

        /*
        Data(type int) holds the
         */
        int data = 9;
        for(int i=0;i<MenuStructure.option_count;i++)
            data = data*10 + MenuStructure.menu_choice[i];

        ImageView imageView = (ImageView) findViewById(R.id.display_qr_iv);
        try {
            generateQRCode_general(""+data, imageView);
        }
        catch(WriterException e) {
            //Handle write exception here
        }
    }
    private void generateQRCode_general(String data, ImageView img)throws WriterException {
        com.google.zxing.Writer writer = new QRCodeWriter();
        String finaldata = Uri.encode(data, "utf-8");

        BitMatrix bm = writer.encode(finaldata, BarcodeFormat.QR_CODE,600, 600);
        Bitmap ImageBitmap = Bitmap.createBitmap(600, 600, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < 600; i++) {//width
            for (int j = 0; j < 600; j++) {//height
                ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK: Color.WHITE);
            }
        }
        if (ImageBitmap != null) {
            img.setImageBitmap(ImageBitmap);
        } else {
            Toast.makeText(getApplicationContext(), "Error at ImageBitmap", Toast.LENGTH_SHORT).show();
        }
    }
}
